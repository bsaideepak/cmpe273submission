package hello
import scala.beans._
import javax.validation.constraints.NotNull

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.index.Indexed;
//import org.springframework.data.mongodb.core.mapping.Document;

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.http.HttpStatus._
import org.springframework.http._
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import java.util.List
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.http.{HttpHeaders, ResponseEntity}
import org.springframework.http.ResponseEntity._
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.ResourceAccessException
import org.springframework.web.client.RestClientException
import org.springframework.web.client.RestOperations
import org.springframework.web.client._

object User {

}

@RunWith(classOf[SpringJUnit4ClassRunner])
@SpringApplicationConfiguration(classes = Array(classOf[Application]))
@WebAppConfiguration
class RoutingInfo {

  @Test
  def testRoute(route: String): String = {
	var certCheck : SSLCertCheck = new SSLCertCheck()	  
  	certCheck.disableChecks()
  	var URL = "https://www.routingnumbers.info/api/data.json?rn="+route
    var restTemplate = new RestTemplate()
    var response = restTemplate.getForEntity(URL, classOf[String])
    var objectMapper: ObjectMapper = new ObjectMapper();
   	var routinginforesponse: JsonNode = objectMapper.readTree(response.getBody());
   	var statusCode = routinginforesponse.get("code").toString()
   	
   	// if (statusCode == "400")
  	//  {
  	  //  var responseHeader: HttpHeaders = new HttpHeaders
  	 //   new ResponseEntity[String]( null, responseHeader, HttpStatus.NOT_FOUND )   
  	 // }
    
   	if (statusCode == "200") {
    		var name = routinginforesponse.get("customer_name").asText()
    		return name
	} 
	else {
		return ("not found")
	}
	return null
  }

}


/*WebLogin Getters and Setters*/

class Web(@NotNull val url : String, val login : String, val pass : String){
  
  private var Id = "p"
  @NotNull
  private var weburl = url
  @NotNull
  private var weblogin = login
  @NotNull
  private var webpassword = pass
  
   def this()={
    this(null, null, null)
  }
  
  
  def setwebId(webId : String) : Unit = {Id = webId  }
  def getwebId : String = {return Id}
  def getweburl : String = {return weburl}
  def getweblogin : String = {return weblogin  }
  def getwebpassword : String = {return webpassword }

}

/*Bank Details*/

@Document(collection = "BankDetails")
class BankDetails(val rnumber : String){
  
  private var zip = "23228"
  @NotNull
  private var bank_name = "Chase"
  @NotNull
  private var city = "San Jose"
  private var Inst_status_code = "1"
  private var address = "1231"
  private var telephone = "2525698"
  private var data_view_code = "1"
  private var state = "VA"
  private var new_rnumber = "000000"
  private var office_code = "O"
    @NotNull
  private var rn = rnumber
   //var bankDetailsmap : Map[String, BankDetails] = Map()
  
   def this()={
    this(null)
  }
  
  
  def setrnumber(rnum : String) : Unit = {rn = rnum}
  
  /*def setcardNumber(cardNumber : String) : Unit = {cardnumber = cardNumber  }
  def getcardId : String = {return Id}
  def getcardnumber : String = {return cardnumber}
  def getexpdate : String = {return expdate  }
  def getcardname : String = {return cardname }*/

}


/* BankDetail getters and setters */

class Bank(val aname : String, val rnum : String, val anum : String){
  
  private var Id = "p"
  @NotNull
  private var accountname = aname
  @NotNull
  private var routingnumber = rnum
  @NotNull
  private var accountnumber = anum
  
   def this()={
    this(null,null,null)
  }
  
  
  def setbankId(cardId : String) : Unit = {Id = cardId  }
  def getcardId : String = {return Id}
  def getaccountname : String = {return accountname}
  def setaccountname(bctemp : String) : Unit = {this.accountname = bctemp}
  def getroutingnumber : String = {return routingnumber  }
  def getaccountnumber : String = {return accountnumber }

}

/*Id card getters and setters*/

class Card(val card_name : String, val card_number : String, val exp_date : String){
  
  private var Id = "p"
  @NotNull
  private var cardname = card_name
  @NotNull
  private var cardnumber = card_number
  private var expdate = exp_date
  
   def this()={
    this(null,null,null)
  }
  
  
  def setcardId(cardId : String) : Unit = {Id = cardId  }
  def setcardNumber(cardNumber : String) : Unit = {cardnumber = cardNumber  }
  def getcardId : String = {return Id}
  def getcardnumber : String = {return cardnumber}
  def getexpdate : String = {return expdate  }
  def getcardname : String = {return cardname }

}

/*user getters and setters*/

class User(val email : String, @NotNull val pass: String) {

  private var User_Id : String = null;
  @NotNull
  private var user_email  = email  
  @NotNull
  private var user_password: String = pass
  
  
  //var usermap : Map[String, Card] = Map()
  var cardmap : Map[String, Card] = Map()
  var webmap : Map[String, Web] = Map()
  var bankmap : Map[String, Bank] = Map()
 // var bankDetailsmap : Map[String, BankDetails] = Map()
  
  private var cardcounter = 0
  private var bankcounter = 0
  private var webcounter = 0
  private var rn =0
  var card = new Card("testName", "333", "12.324.5")
  var web = new Web("testName", "333", "12.324.5")
  var bank = new Bank("testName", "333", "12.324.5")
  //var bankDetails = new BankDetails("123333")
  private var created_at : String = null 
  
  def this()={
    this(null,null)
  }
  
  
  def getUserId : String = (return User_Id)
  def getUser_password : String = { return user_password}
  def getUser_email : String = { return user_email}
  def getcreated_at : String = {return created_at}
  def setcreated_at(currenttime : String) : Unit = {created_at = currenttime}
  def setUser_email(email : String) : Unit = {this.user_email = email}
  def setUser_Pass(pass : String) : Unit = {this.user_password = pass}
  def setUserId (Id : String): Unit = {User_Id = Id}
  
  
  
  
  
  
  /*Create Icardmap*/
    
  def makecardmap (usercard : Card) : Card = {
    this.card = usercard
	card.setcardNumber(usercard.getcardnumber)
    this.cardcounter += 1
    var cardId = "c-" + this.cardcounter.toString()
    card.setcardId(cardId)
    this.cardmap = cardmap + (cardId -> card)
    return this.card
  }
  
  /*Create Weblogin Map*/
  
  def makewebmap (userweb : Web) : Web = {
    web = userweb
	this.webcounter += 1
    var webId = "l-" + webcounter.toString()
    web.setwebId(webId)
    webmap = webmap + (webId -> web)
    return web
  }
  
  /*Create Bank Map*/
  
  def makebankmap (userbank : Bank) : Bank = {
    bank = userbank
	this.bankcounter += 1
    var bankId = "b-" + bankcounter.toString()
    bank.setbankId(bankId)
    bankmap = bankmap + (bankId -> bank)
    return bank
  }
  
 /* def makebankDetailsmap (bd : BankDetails) : BankDetails = {
    this.bankDetails = bd
    this.rn += 1
    var rnumber = "rn-" + rn.toString()
    bankDetails.setrnumber(rnumber)
    bankDetailsmap = bankDetailsmap + (rnumber -> bankDetails)
    return bankDetails
  }*/
  

}