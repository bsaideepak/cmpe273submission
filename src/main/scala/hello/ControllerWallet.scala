package hello

import org.springframework.context.annotation.Configuration
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.joda.time.DateTime
import org.springframework.context.annotation.ComponentScan
import org.springframework.stereotype._
import org.springframework.boot.autoconfigure._
import org.springframework.web.bind.annotation._
import java.util.concurrent.atomic.AtomicLong
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.http.HttpStatus
import collection.JavaConversions._
import java.util.ArrayList
import collection.JavaConversions._
import javax.validation.Valid                          // import this for validation
import org.springframework.validation.BindingResult
import org.springframework._
import org.springframework.data.mongodb._
//import org.springframework.data.mongodb.
import org.springframework.data.mongodb.core.MongoOperations;

import org.springframework.data.mongodb.core.query.Criteria

import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.config.RepositoryConfigurationSourceSupport
import org.springframework.data.domain.Sort;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


import java.lang.annotation.Annotation;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.boot.autoconfigure.AutoConfigurationPackages;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;

import org.springframework.data.repository.config.AnnotationRepositoryConfigurationSource;
import org.springframework.data.repository.config.RepositoryConfigurationDelegate;
import org.springframework.data.repository.config.RepositoryConfigurationExtension;

import org.springframework.data.repository.Repository

import com.mongodb.BasicDBObject


import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;


import com.mongodb.BasicDBObject
import com.mongodb.DB
import com.mongodb.DBCollection
import com.mongodb.DBCursor
import com.mongodb.DBObject
import com.mongodb.Mongo
import com.mongodb.MongoException

//import hello.MongoConfig



//import com.mongodb._


@RestController
@Configuration
@EnableAutoConfiguration
@ComponentScan
class ControllerWallet {
  

  var id_counter = 0
  var user = new User();
  var user_map: Map[String, User] = Map();
  //var bank_map: Map[String, BankDetails] = Map();
  
  
  
   /*def makeusermap (user1 : ) : Card = {
    this.card = usercard
	card.setcardNumber(usercard.getcardnumber)
    this.cardcounter += 1
    var cardId = "c-" + this.cardcounter.toString()
    card.setcardId(cardId)
    this.cardmap = cardmap + (cardId -> card)
    return this.card
  }*/
  
  
 /* Create MongoDB Connection */
  
  
/*Create User-POST request*/
  
 @RequestMapping(value = Array("/api/v1/users"),method=Array(RequestMethod.POST),headers=Array("content-type=application/json"), consumes = Array("application/json"))
   def userCreation(@Valid @RequestBody user : User,result: BindingResult):User = {
   
		    if (result.hasErrors()) {
		    	throw new ParameterMissingException(result.toString)
    } 
    else 
    {
      
      val ctx = new AnnotationConfigApplicationContext(classOf[MongoConfig])
      val mongoOperation = ctx.getBean("mongoTemplate").asInstanceOf[MongoOperations]
      
      
    	id_counter = id_counter+1
    	this.user = user
    	
    	
    	var user_id = "U-" + id_counter.toString();
      	
    	user.setUserId(user_id)
    	
    	
    	
    	val currentTime = DateTime.now;
    	
    	
    	
        this.user.setcreated_at(currentTime.toString);
       
        
    	
    	this.user_map = this.user_map + (user_id -> this.user)
    	//mongoOperation.insert(user.);
    	//mongoOperation.insert(user.getUser_password)
    	
    	
    	mongoOperation.save(user_map);
    	
    	
    	
    	return user
    	
    }
 
 }

 
 /*View User-GET request*/
 
  @RequestMapping(value=Array("/api/v1/users/{userid}"), method=Array(RequestMethod.GET), produces = Array("application/json"), headers=Array("content-type=application/json"))
	def viewuser(@PathVariable("userid")  userId:String ):User={
		var view_user = user_map(userId)
		return view_user;
	} 
  
  
  /*Update user-PUT request*/
  
    @RequestMapping(value = Array("/api/v1/users/{userid}"), method = Array(RequestMethod.PUT), headers = Array("content-type=application/json"), consumes = Array("application/json"))
		def upduser(@PathVariable("userid")  userId:String ,@RequestBody user : User ):User={
      var update_user = new User()
      user.setUserId(userId)
      this.user_map = this.user_map + (userId -> user)
		update_user = user_map(userId)
		return update_user
		
    }
    
     /*Create Icards-POST request*/
    
   @RequestMapping(value = Array("/api/v1/users/{userid}/idcards"), method = Array(RequestMethod.POST), headers = Array("content-type=application/json"), consumes = Array("application/json"))
       def idcards( @PathVariable("userid")  userId:String ,@Valid @RequestBody usercard : Card,result:BindingResult): Card = {
     if (result.hasErrors()) {
		    	throw new ParameterMissingException(result.toString)
    } 
    else 
    {
      val ctx = new AnnotationConfigApplicationContext(classOf[MongoConfig])
      val mongoOperation = ctx.getBean("mongoTemplate").asInstanceOf[MongoOperations]
      
     var icard = user_map(userId) 
    		     icard.makecardmap(usercard)
    		   this.user_map += (userId -> icard)
    		   mongoOperation.save(user.cardmap);
    		   return icard.card
    }
   }
    
   /*View Icards-GET request*/
            
   @RequestMapping(value=Array("/api/v1/users/{userid}/idcards"), method=Array(RequestMethod.GET), produces = Array("application/json"), headers=Array("content-type=application/json"))
	def viewcards(@PathVariable("userid")  userId:String ):java.util.Map[String,Card]={
    var view_icard = user_map(userId)
    return view_icard.cardmap
   }
   
   /*Delete Icard-DELETE*/
   
   @RequestMapping(value=Array("/api/v1/users/{userid}/idcards/{card_id}"), method=Array(RequestMethod.DELETE), headers=Array("content-type=application/json"))@ResponseStatus(HttpStatus.NO_CONTENT)
	def deletecards(@PathVariable("userid")  userId:String, @PathVariable("card_id")  cardId:String ):Unit={
     var u = user_map(userId)
     u.cardmap -= cardId
     user_map += (userId -> u)
   }
   
   /*Create Weblogins*/
   
   @RequestMapping(value = Array("/api/v1/users/{userid}/weblogins"), method = Array(RequestMethod.POST), headers = Array("content-type=application/json"), consumes = Array("application/json"))
       def webloginscards( @PathVariable("userid")  userId:String ,@Valid @RequestBody userlogin : Web,result:BindingResult): Web = {
      if (result.hasErrors()) {
		    	throw new ParameterMissingException(result.toString)
    } 
    else 
    { 
       val ctx = new AnnotationConfigApplicationContext(classOf[MongoConfig])
      val mongoOperation = ctx.getBean("mongoTemplate").asInstanceOf[MongoOperations]
     var u = user_map(userId)
    	u.makewebmap(userlogin)
    	this.user_map += (userId -> u)
    	mongoOperation.save(user.webmap);
    	return u.web
    }  
   }
   
   /*View Logins*/
   
   @RequestMapping(value=Array("/api/v1/users/{userid}/weblogins"), method=Array(RequestMethod.GET), produces = Array("application/json"), headers=Array("content-type=application/json"))
	def viewweb(@PathVariable("userid")  userId:String ):java.util.Map[String,Web]={
    var u = user_map(userId)
    return u.webmap
   }
   
   /*DELETE logins*/
   
   @RequestMapping(value=Array("/api/v1/users/{userid}/weblogins/{login_id}"), method=Array(RequestMethod.DELETE), headers=Array("content-type=application/json"))@ResponseStatus(HttpStatus.NO_CONTENT)
	def deleteweb(@PathVariable("userid")  userId:String, @PathVariable("login_id")  loginId:String ):Unit={
     var u = user_map(userId)
     u.webmap -= loginId
     user_map += (userId -> u)
   }
   
   /*Create BankAccounts*/
   
    @RequestMapping(value = Array("/api/v1/users/{userid}/bankaccounts"), method = Array(RequestMethod.POST), headers = Array("content-type=application/json"), consumes = Array("application/json"))
       def userbank( @PathVariable("userid")  userId:String ,@Valid @RequestBody bank : Bank,result:BindingResult): Bank = {
       if (result.hasErrors()) {
		    	throw new ParameterMissingException(result.toString)
    } 
    else 
    { 
      val bc = new RoutingInfo
      var bctemp = bc.testRoute(bank.getroutingnumber)
      println(bctemp)
      
      val ctx = new AnnotationConfigApplicationContext(classOf[MongoConfig])
      val mongoOperation = ctx.getBean("mongoTemplate").asInstanceOf[MongoOperations]
      bank.setaccountname(bctemp)
     // println( ""+(bank.getaccountname()).)
      
      var u = user_map(userId)
    		     u.makebankmap(bank)
    		    this.user_map += (userId -> u)
    		    
    		    mongoOperation.save(user.bankmap);
    		     return u.bank
    }  
       
       
       
    }
    
  /*  @RequestMapping(value = Array("/api/v1/{userid}/newBank"), method = Array(RequestMethod.POST), headers = Array("content-type=application/json"), consumes = Array("application/json"))
       def newBank(@PathVariable("userid")  userId:String ,@Valid @RequestBody bankDetails : BankDetails,result:BindingResult): BankDetails = {
       if (result.hasErrors()) {
		    	throw new ParameterMissingException(result.toString)
    } 
    else 
    { 
      val ctx = new AnnotationConfigApplicationContext(classOf[MongoConfig])
      val mongoOperation = ctx.getBean("mongoTemplate").asInstanceOf[MongoOperations]
      var u = user_map(userId)
      u.makebankDetailsmap(bankDetails)
      this.user_map += (userId -> u)
    		    //mongoOperation.save(user.bankmap);
	  return u.bankDetails
    }  
    }*/
    
     /*@RequestMapping(value = Array("/api/new_bank"),method=Array(RequestMethod.POST),headers=Array("content-type=application/json"), consumes = Array("application/json"))
       	def newBank(@Valid @RequestBody bankDetails : BankDetails,result: BindingResult):BankDetails = {
   
		    if (result.hasErrors()) {
		    	throw new ParameterMissingException(result.toString)
    } 
    else */
    
    
    
   
    
    /*get Bank Details Create */
    
 /*@RequestMapping(value=Array("/api/data.json/{rn}"), method=Array(RequestMethod.GET), produces = Array("application/json"), headers=Array("content-type=application/json"))
	def bankview(@PathVariable("rn")  rn:String ):DBObject={
   println("yo1" + rn)
   
   val ctx = new AnnotationConfigApplicationContext(classOf[MongoConfig])
      val mongoOperation = ctx.getBean("mongoTemplate").asInstanceOf[MongoOperations]
   println("yo2")
      
  val query2 = new Query()
    query2.addCriteria(Criteria.where("rn").is("12345"))
    
    val mongo = new Mongo("localhost", 27017)
      val db = mongo.getDB("assignment2cmpe273")
      val collection = db.getCollection("bank")
      val userCollection = db.getCollection("map1")
      
      
     val dbObject = collection.findOne()
    
     var name = dbObject.get("bank_name");
   	
   	val dbObject1 = userCollection.findOne()
   	
   	userCollection.update((((db.getCollection("map1")).findOne("value1"))), ((db.getCollection("bank")).findOne("bank_name")))
   	
   	
   		//println(name);
   
      
  val query1 = new BasicQuery("{ rn : '12345'}")
   val userTest1 = mongoOperation.findOne(query1, classOf[BankDetails])
   println("yo3")
   return dbObject
   }
    */
     
    
    //View BankAccounts
     
   @RequestMapping(value=Array("/api/v1/users/{userid}/bankaccounts"), method=Array(RequestMethod.GET), produces = Array("application/json"), headers=Array("content-type=application/json"))
	def viewbank(@PathVariable("userid")  userId:String ):java.util.Map[String,Bank]={
    var u = user_map(userId)
    return u.bankmap
   }
   
   //DELETE BankAccounts
   
   @RequestMapping(value=Array("/api/v1/users/{userid}/bankaccounts/{ba_id}"), method=Array(RequestMethod.DELETE), headers=Array("content-type=application/json"))@ResponseStatus(HttpStatus.NO_CONTENT)
	def deletebank(@PathVariable("userid")  userId:String, @PathVariable("ba_id")  baId:String ):Unit={
     var u = user_map(userId)
     u.bankmap -= baId
     user_map += (userId -> u)
   }
}
  