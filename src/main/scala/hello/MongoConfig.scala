package hello

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.authentication.UserCredentials
import com.mongodb.MongoClient
//remove if not needed
import scala.collection.JavaConversions._

@Configuration
class MongoConfig {

  @Bean
  def mongoTemplate(): MongoTemplate = {
    val mongoTemplate = new MongoTemplate(new MongoClient("ds049160.mongolab.com:49160"), "cmpe273sjsu", new UserCredentials("saideepak09","MongoDB09!;"))
    mongoTemplate
  }
}